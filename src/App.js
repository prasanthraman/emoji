
import './App.css';
import React from 'react'
import axios from 'axios'


import Loader from './components/Loader/Loader'
import Card from './components/Card/Card'




class App extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      data:null,
      dataFetched: false,
      apiFailed: false,
      filterList:[],
      // data:[{"name":"grinning face","category":"smileys and people","group":"face positive","htmlCode":["\u0026#128512;"],"unicode":["U+1F600"]},{"name":"grinning face with smiling eyes","category":"smileys and people","group":"face positive","htmlCode":["\u0026#128513;"],"unicode":["U+1F601"]},{"name":"face with tears of joy","category":"smileys and people","group":"face positive","htmlCode":["\u0026#128514;"],"unicode":["U+1F602"]}]
    }
  }

  componentDidMount() {
    console.log("Fetch Init")
    this.fetchdata()
  }

  fetchdata = () => {
    console.log("Request Initiated")
    axios.get('https://emojihub.yurace.pro/api/all')
      .then((response) => {
        console.log(response.data)
        this.setState({
          data: response.data,
          dataFetched: true,
     
        })
      })
      .catch((err) => {
        this.setState({
          apiFailed: true
        })
      })
  }

  render() {
   



    let categories=[]
  
    let handleChange =(e)=>{
      console.log(e.target.id)
      let filterList=[]
      if(this.state.filterList.length > 0){

         filterList=[...this.state.filterList]
      }
      let index = filterList.indexOf(e.target.id);

      if (index > -1) { 
        filterList.splice(index,1)
        this.setState({filterList:filterList}) 
      }else{
        filterList.push(e.target.id)
        this.setState({filterList:filterList})
      }

     
    //  let entries= document.querySelectorAll(`.${category}`)
    //  entries.array.forEach(element => {
    //   console.log(element)
    //  });
    }
    return (
      <div className="App">
        <div className='row Container'>
          {console.log("FETCHHH")}
          {console.log(this.state.filterList,"hghhh")}
          {this.state.dataFetched ?
           <div className='main-container'>
            <div className='right-container'>
              {this.state.data.map((emoji)=>{
              
              if(!(categories.includes(emoji.category))){
                categories.push(emoji.category)

              }

              if(this.state.filterList.length>0){

                if(this.state.filterList.includes(emoji.category)){
                  return <Card
                  name={emoji.name} 
                  htmlCode={emoji.htmlCode}
                  category={emoji.category}
                  group={emoji.group}
                  unicode={emoji.unicode}
                  />
                }else{
                  return<></>
                }

              }else{

                return <Card
                name={emoji.name} 
                htmlCode={emoji.htmlCode}
                category={emoji.category}
                group={emoji.group}
                unicode={emoji.unicode}
                />
              }
            })} 
            </div>

            <div className='left-container'> 

            {categories.map((category)=>{
            return    <label><input type="checkbox" name={category} id={category} onChange={handleChange}/> {category} </label>
           })}
             </div>
            

            </div>

            : this.state.apiFailed ? "Sorry Failed to Fetch data!" :
              <Loader className="loader" />}
        </div>




      </div>
    )
  }
}

export default App

