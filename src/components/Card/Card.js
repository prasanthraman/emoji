import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';

import "./Card.css"
export class Card extends Component {
    constructor(props){
        super(props)
        this.state={
          isCopied:false,
          copySuccess:'',
        }

      

    }

  render() {

    let backtoNormal = ()=>{

      setTimeout(()=>{

        this.setState({isCopied:false})
      },3000)
    }
    return (
      
      <div className={'emoji-container Card row'+ this.props.category}>
       
          <div className='emoji' dangerouslySetInnerHTML={{__html: `${this.props.htmlCode}`}} />
       <button onClick={() => {
        navigator.clipboard.writeText(this.props.htmlCode)
        this.setState({isCopied:true})
        backtoNormal()
        }} className={this.state.isCopied?"copied":""} > Copy to clipboard</button>
       <div className='emoji-description'>
        <h2 className='title'> Name: <span className='title-value'>{this.props.name}</span> </h2>
        <h2 className='title'> Category:  <span className='title-value'>{this.props.category}</span> </h2>
        <h2 className='title'> Group:  <span className='title-value'>{this.props.group}</span> </h2>

       </div>

      </div>
    
    )
  }
}

export default Card